﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using unirest_net.http;

namespace Obrecon.Web.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Redirect()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Stream()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Capture()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CompareImage()
        {
            var posted = Request.Form["imgBase64"];

            posted = posted.Replace("data:image/jpeg;base64,", "");
            posted = posted.Replace(" ", "+");

            var imageName = DateTime.Now.ToString().Replace("/", "-").Replace(" ", "- ").Replace(":", "").Replace(" ", "") + ".jpg";

            try
            {
                SaveImage(imageName, posted);
            }
            catch (Exception ex)
            {
                return Json(new { error = new { message = ex.Message } });
            }

            try
            {
                string result = null;

                if (AppSettings.TestMode == 1)
                    result = @"{""results"": [{""item"": {""content"": null, ""uuid"": ""5e58aa4d8b764f78a100107392c8bbc3"", ""name"": ""Mueble Estantería""}, ""image"": {""thumb_120"": ""Content/images/mueble.jpg"", ""uuid"": ""bb59bbd4a1824caf9952694283b9eec6""}, ""score"": 28}]}";
                else if (AppSettings.TestMode == 2)
                    result = @"{""results"": [{""item"": {""content"": null, ""uuid"": ""5e58aa4d8b764f78a100107392c8bbc3"", ""name"": ""Mueble Estantería""}, ""image"": {""thumb_120"": ""Content/images/mueble.jpg"", ""uuid"": ""bb59bbd4a1824caf9952694283b9eec6""}, ""score"": 28}, {""item"": {""content"": null, ""uuid"": ""5e58aa4d8b764f78a100107392c8bbc3"", ""name"": ""Mueble Estantería""}, ""image"": {""thumb_120"": ""https://crs-beta-catchoom.s3.amazonaws.com/cache/collections/0f1b37888a344b8a904b6de8d848816d/images/5e58aa4d8b764f78a100107392c8bbc3_bb59bbd4a1824caf9952694283b9eec6_thumb_120.jpe"", ""uuid"": ""bb59bbd4a1824caf9952694283b9eec6""}, ""score"": 28}]}";
                else
                    result = UploadImage(imageName, posted);

                return Content(result, "application/json");
            }
            catch (Exception ex)
            {
                return Json(new { error = new { message = ex.Message } });
            }
        }

        private void SaveImage(string imageName, string postedImage)
        {
            var savePath = Server.MapPath("~/App_Data/ImagesToScan/");
            var fileNameWitPath = Path.Combine(savePath, imageName);

            using (var fileStream = new FileStream(fileNameWitPath, FileMode.Create))
            {
                using (var binaryWriter = new BinaryWriter(fileStream))
                {
                    var dataBytes = Convert.FromBase64String(postedImage);
                    binaryWriter.Write(dataBytes);
                    binaryWriter.Close();
                }
            }
        }

        private string UploadImage(string imageName, string postedImage)
        {
            var response = Unirest.post("https://r.catchoom.com/v1/search")
              .field("image", Convert.FromBase64String(postedImage))
              .field("token", AppSettings.CollectionToken)
              .asString();

            return response.Body;
        }

        private string TestUpload()
        {
            var response = Unirest.post("https://r.catchoom.com/v1/timestamp")
              .field("token", AppSettings.CollectionToken)
              .asString();

            return response.Body;
        }
    }
}
