﻿$(function () {
    $('#response-modal').modal({ show: false });
});
function makeAjaxRequest(options) {
    $('.result-div').hide();
    $('#response-loading').show();
    $('#response-modal').modal('show');
    $.ajax({
        url: options.url,
        type: 'POST',
        dataType: 'json',
        data: { 'imgBase64': options.data },
        complete: function (xhr, textStatus) {
            // Request complete.
        },
        // Request was successful.
        success: function (response, textStatus, xhr) {
            console.log('Response: ', response);
            // Comparison successful.
            handleAjaxResponse(response);
        },
        error: function (xhr, textStatus, errorThrown) {
            // Some error occured.
            console.log('Error: ', errorThrown);
        }
    });
}
function handleAjaxResponse(response) {
    $('#response-loading').hide();
    var responseToShow = null;
    if (response.results) {
        if (response.results.length > 0) {
            var topResult = response.results[0];
            responseToShow = $('#response-success');
            var text = null;
            if (response.results.length == 1) {
                text = 'Hay un ítem reconocible, se llama "{0}" y su puntaje es {1}.'.replace('{0}', topResult.item.name).replace('{1}', topResult.score);
            }
            else {
                text = 'Hay {0} ítems reconocibles. Aquel con el puntaje más alto ({1}) es "{2}".'.replace('{0}', response.results.length).replace('{1}', topResult.score).replace('{2}', topResult.item.name);
            }
            responseToShow.find('h4').html(text);
            responseToShow.find('div').html('<img src="{0}" alt="Best matching reference image" />'.replace('{0}', topResult.image.thumb_120));
        }
        else {
            responseToShow = $('#response-failure');
        }
    }
    else if (response.error) {
        responseToShow = $('#response-error');
        responseToShow.find('h4').html('Ocurrió un error: "{0}".'.replace('{0}', response.error.message));
    }
    $(responseToShow).show();
}