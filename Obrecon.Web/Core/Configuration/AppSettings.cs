﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Obrecon.Web
{
    public class AppSettings
    {
        public static string CollectionToken
        {
            get
            {
                return GetConfigSetting("CollectionToken");
            }
        }
        public static int TestMode
        {
            get
            {
                return GetIntegerConfigSetting("TestMode");
            }
        }

        private static string GetConfigSetting(string settingName)
        {
            if ((ConfigurationManager.AppSettings != null) && (ConfigurationManager.AppSettings.Count != 0))
            {
                try
                {
                    var _value = ConfigurationManager.AppSettings.Get(settingName);
                    if (_value == null)
                    {
                        throw new ConfigurationErrorsException("Invalid configuration setting: " + settingName);
                    }
                    return _value;
                }
                catch (Exception)
                {
                    throw new ConfigurationErrorsException("Invalid configuration setting: " + settingName);
                }
            }
            else
            {
                throw new ConfigurationErrorsException("Invalid configuration. Required AppSettings section is missing.");
            }
        }
        private static int GetIntegerConfigSetting(string settingName)
        {
            if ((ConfigurationManager.AppSettings != null) && (ConfigurationManager.AppSettings.Count != 0))
            {
                try
                {
                    var _value = ConfigurationManager.AppSettings.Get(settingName);
                    if (_value == null)
                    {
                        throw new ConfigurationErrorsException("Invalid configuration setting: " + settingName);
                    }
                    return (int.Parse(_value));
                }
                catch (Exception)
                {
                    throw new ConfigurationErrorsException("Invalid configuration setting: " + settingName);
                }
            }
            else
            {
                throw new ConfigurationErrorsException("Invalid configuration. Required AppSettings section is missing.");
            }
        }
        private static bool GetBooleanConfigSetting(string settingName)
        {
            if ((ConfigurationManager.AppSettings != null) && (ConfigurationManager.AppSettings.Count != 0))
            {
                try
                {
                    var _value = ConfigurationManager.AppSettings.Get(settingName);
                    if (_value == null)
                    {
                        throw new ConfigurationErrorsException("Invalid configuration setting: " + settingName);
                    }
                    return (bool.Parse(_value));
                }
                catch (Exception)
                {
                    throw new ConfigurationErrorsException("Invalid configuration setting: " + settingName);
                }
            }
            else
            {
                throw new ConfigurationErrorsException("Invalid configuration. Required AppSettings section is missing.");
            }
        }
    }
}