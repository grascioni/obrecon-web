﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Obrecon.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Stream", "media-stream",
                defaults: new { controller = "Home", action = "Stream", step = UrlParameter.Optional });

            routes.MapRoute("Capture", "media-capture",
                defaults: new { controller = "Home", action = "Capture", step = UrlParameter.Optional });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Redirect", id = UrlParameter.Optional }
            );
        }
    }
}